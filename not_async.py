import asyncio


async def double() -> None:
    for n in range(1, 7):
        print(f'double: {n * 2}')
        await asyncio.sleep(0.6)


async def fibonacci() -> None:
    series = [1, 1]
    while series[-1] < 10:
        print(f'fibonacci: {series[-1]}')
        series.append(series[-2] + series[-1])
        await asyncio.sleep(1)


async def main():
    await double()
    await fibonacci()


asyncio.run(main())

