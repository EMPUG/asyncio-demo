import time

from bs4 import BeautifulSoup
import requests


def get_domain_list() -> list[str]:
    # Read the list of domains from domains.txt.
    domains = []
    with open('domains.txt') as f:
        for line in f:
            line = line.strip()
            if line:
                domains.append(line)
    return domains


def print_domain_info(domain: str):
    r = requests.get(domain)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(f'{r.status_code}: {domain}: {soup.title.string}')


def main():
    start_time = time.time()
    domains = get_domain_list()
    for domain in domains:
        # Fetch the information for each domain sequentially.
        print_domain_info(domain)
    end_time = time.time()

    print()
    print(f'Duration: {(end_time - start_time):.3f} seconds')


if __name__ == '__main__':
    main()
