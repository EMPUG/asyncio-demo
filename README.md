# Python Async IO

## Definitions

_Concurrency_ means that multiple tasks are run in an overlapping manner.

_Multiprocessing_ spreads tasks over a computer’s CPUs or cores. Each task runs
in it's own process. It's suited for CPU bound tasks. See Python's
[multiprocessing](https://docs.python.org/3/library/multiprocessing.html)
library.

With _threading_, multiple threads take turns executing tasks. One process can
contain multiple threads. Threading is suited for IO-bound tasks. See Python's
[threading](https://docs.python.org/3/library/threading.html) library.

_Async IO_ implements concurrency using cooperative multitasking. Tasks, or
coroutines, run in a single thread in a single process. Aysnc IO is suited for
IO-bound tasks. See Python's [asyncio](https://docs.python.org/3/library/asyncio.html)
library.

## Python asyncio

In Python, asynchrounous code is built using coroutines (or asynchronous generators)
which can yield when they are waiting.

Use `async` to indicate that a function is a coroutine.

Use `await` when calling a coroutine.

Typically, the `main()` function has the `async` keyword, and is called with

        asyncio.run(main())

Tasks are used to schedule coroutines to run concurrently. Tasks are created
by calling `asyncio.create_task()`.

A group of tasks can be run concurrently with `asyncio.wait()`, `asyncio.wait_for()`,
`asyncio.gather()`, and `asyncio.as_completed()`.

Tasks can be cancelled with the
[cancel()](https://docs.python.org/3/library/asyncio-task.html#asyncio.Task.cancel)
method.

[Synchronization Primitives](https://docs.python.org/3/library/asyncio-sync.html)
allow tasks to communicate with each other.

## Code examples

[simple_example.py](simple_example.py) is a simple example of asyncio with two
coroutines.

[not_async.py](not_async.py) does not run asynchronously, because the coroutines
run sequentially.

[domain_info.py](domain_info.py) gets information from a list of domains in a
non-asynchronous (sequential) manner.

[domain_info_async.py](domain_info_async.py) gets the domain information
asynchronously.

[domain_info_async2.py](domain_info_async2.py) demonstrations how data can be
returned from asynchronous functions using the `asyncio.gather()` function.

## Notes

Python's asyncio library has changed dramatically and has gotten easier to work
with. When looking at example code on the internet, look for code based on a
recent version of Python.

Note that, by default, Python's logging does not run asynchronously. It will
block while it writes to logs. Consider using a logging queue in a separate
thread to improve performance. See 
[Logging in asyncio applications](https://www.zopatista.com/python/2019/05/11/asyncio-logging/)
for an example.

## References

The [asyncio library documentation](https://docs.python.org/3/library/asyncio.html)
from the Python standard library

[Async IO in Python: A Complete Walkthrough](https://realpython.com/async-io-python/)
