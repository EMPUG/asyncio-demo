import aiohttp
import asyncio
import time
import platform

from bs4 import BeautifulSoup


def get_domain_list() -> list:
    # Read the list of domains from domains.txt.
    domains = []
    with open('domains.txt') as f:
        for line in f:
            line = line.strip()
            if line:
                domains.append(line)
    return domains


async def print_domain_info(domain: str) -> None:
    async with aiohttp.ClientSession() as session:
        async with session.get(domain) as r:
            html = await r.text()
            soup = BeautifulSoup(html, 'html.parser')
            print(f'{r.status}: {domain}: {soup.title.string}')


async def main():
    start_time = time.time()
    domains = get_domain_list()
    tasks = []
    for domain in domains:
        # Create a task to fetch data from each domain.
        task = asyncio.create_task(print_domain_info(domain))
        tasks.append(task)

    # Run the tasks asynchronously.
    await asyncio.wait(tasks)
    end_time = time.time()

    print()
    print(f'Duration: {(end_time - start_time):.3f} seconds')

# fix nuisance warnings on Windows OS
myOS = platform.system()
if (myOS[0:1] == "W"):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
asyncio.run(main())
